![preview picture](http://bit-fridge.net/meliora.png "Meliora Preview")

# Meliora #
## An Openbox window manager theme family. ##
### Created by Cory Swauger <ensayia@gmail.com> ###

---

## Why Meliora? ##

I've spent a lot of time picking through available openbox themes in various locations. I could
never find anything that was a good balance between **usability** and **simplicity**. Most themes were
wacky over-the-top gaudy or had microscopic title bars and icons far too uncomfortable for a
high-resolution workspace. Rather than keep a custom copy of an existing theme up to date, I 
created this theme to achive a balance of **aesthetic** and **ease of use** at modern desktop resolutions.

I hope you find Meliora to your liking. Please feel free to email me with questions or concerns,
or simply leave and issue in the tracker.

## How to Use ##

Arch Linux Users: meliora-openbox-themes is available in the **AUR**.

Everyone Else:

+ Clone the repo:
    - `git clone git@bitbucket.org:Ensayia/meliora-openbox-themes.git`
+ Change to the repo directory:
    - `cd meliora-openbox-themes`
+ Copy the files to your theme folder:
    - `touch ~/.themes`
    - `cp -R * ~/.themes`
+ Select the theme with an openbox configuration utility. `obconf` and `lxappearance` are available on almost every linux distro.
+ Done!
+ If you want to remove the themes:
    - `rm -r ~/.themes/Meliora-*`